//////////////////////////////////////////////////////
//  parrotARDroneOuts.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Dec 23, 2013
//      Author: joselusl
//
//  Description: file to manage ardrone_autonomy node
//  See: https://github.com/AutonomyLab/ardrone_autonomy
//
//////////////////////////////////////////////////////


#ifndef _PELICAN_OUTS_H
#define _PELICAN_OUTS_H


#include "droneModuleROS.h"
#include "communication_definition.h"
//Altitude and Ground Speed
#include "px_comm/OpticalFlow.h"
#include "droneMsgsROS/droneAltitude.h"
#include "droneMsgsROS/vector2Stamped.h"

//// ROS  ///////
#include "ros/ros.h"

//I/O stream
//std::cout
#include <iostream>

//Math
//M_PI
#include <cmath>

// low pass filter
#include "control/LowPassFilter.h"
#include "xmlfilereader.h"
#include "control/filtered_derivative_wcb.h"

#define PX4FLOW_INT_FDWCB_PRE_TR       (  0.020                            )
#define PX4FLOW_INT_FDWCB_POST_TR      (  0.020                            )
#define PX4FLOW_INT_FDWCB_TDERIV       (  0.200                            )
#define PX4FLOW_INT_FDWCB_TMEMORY      (  5.000 * PX4FLOW_INT_FDWCB_TDERIV )
#define PX4FLOW_INT_FDWCB_SENSORFREQ   (100.000                            )

/////////////////////////////////////////
// Class Altitude
//
//   Description
//
/////////////////////////////////////////
class AltitudeROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher AltitudePubl;
    bool publishAltitudeValue();
    CVG_BlockDiagram::LowPassFilter h_lowpassfilter;


    //Subscriber
protected:
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;
    ros::Subscriber AltitudeSubs;
    void altitudeCallback(const px_comm::OpticalFlow::ConstPtr& msg);


    //Altitude msgs
protected:

    droneMsgsROS::droneAltitude AltitudeMsgs;


    //Constructors and destructors
public:
    AltitudeROSModule();
    ~AltitudeROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

/////////////////////////////////////////
// Class GroundSpeed
//
//   Description:
//
/////////////////////////////////////////
class GroundSpeedROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher GroundSpeedPubl;
    bool publishGroundSpeedValue();
    CVG_BlockDiagram::LowPassFilter vx_lowpassfilter, vy_lowpassfilter;

    //Subscriber
protected:
    ros::Subscriber GroundSpeedSubs;
    void groundSpeedCallback(const px_comm::OpticalFlow::ConstPtr& msg);


    //GroundSpeed msgs
protected:
    droneMsgsROS::vector2Stamped GroundSpeedMsgs;


    //Constructors and destructors
public:
    GroundSpeedROSModule();
    ~GroundSpeedROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};




#endif
